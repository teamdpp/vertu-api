# Vertu Real-Time Email Web Service

###This repository is used for sending real-time email notifications to Vertu customers.

##Setup

####JSON Data Example

```json
{
	"email_type":1,
	"order_number":"029473-38X34",
	"customer_name":"John Appleseed",
	"customer_email":"customer@email.com",
	"receipt_url":"http://vertumotors.com",
	"order_summary_url":"http://vertumotors.com",
	"confirmation_url":"http://vertumotors.com",
	"delivery_selection":"You have chosen to have the vehicle delivered to your home.",
	"vehicle":
	{ 
		"manufacturer":"Infiniti",
		"model":"Q50",
		"variant":"3.0T Sport Tech 4dr Auto Petrol Saloon",
		"registration":"QW12RTY", 
		"price":19999,
		"transmission":"Manual",
		"fuel":"Petrol",
		"body":"Hatchback",
		"year":"07/57 Reg",
		"mileage":69502,
		"colour":"blue",
		"image_url":"http://placehold.it/250x250"
	},
	"part_exchange":
	{ 
		"registration":"NE11 OXA",
		"valuation":9999,
		"settlement":9500
	},
	"accessories":
	[
		"Diamondbrite",
		"Carpet Mats",
		"Mud Flaps"
	]
}
```

Send your built JSON request to the API here 

```
http://vertu.dpp-host.co.uk/api/send-email
```

####PHP Curl Example

```php
<?php
$data = [
	"email_type" => 1,
	"order_number" => "029473-38X34",
	"customer_name" => "John Appleseed",
	"customer_email" => "customer@email.com",
	"receipt_url" => "http://vertumotors.com",
	"order_summary_url" => "http://vertumotors.com",
	"confirmation_url" => "http://vertumotors.com",
	"delivery_selection" => "You have chosen to have the vehicle delivered to your home.",
	"vehicle" =>
		[
			"manufacturer" => "Infiniti",
			"model" => "Q50",
			"variant" => "3.0T Sport Tech 4dr Auto Petrol Saloon",
			"registration" => "QW12RTY", 
			"price" => 19999,
			"transmission" => "Manual",
			"fuel" => "Petrol",
			"body" => "Hatchback",
			"year" => "07/57 Reg",
			"mileage" => 69502,
			"colour" => "blue",
			"image_url" => "http://placehold.it/250x250"
		],
	"part_exchange" =>
		[
			"registration" => "NE11 OXA",
			"valuation" => 9999,
			"settlement" => 9500
		],
	 "accessories" =>
	 [
		"Diamondbrite",
		"Carpet Mats",
		"Mud Flaps"
	 ]
];

$data = json_encode($data);
																						 
$ch = curl_init('http://vertu.dpp-host.co.uk/api/send-email');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-type: application/json',
	'Accept: application/json',
	'Authorization: Bearer {api_token}'
));

$result = curl_exec($ch);

print_r(json_decode($result));
?>
```